This is an English version of the [README_CN.md](README_CN.md) documentation.

# IDArling Deploy Helper

Quickly deploy idarling server and management interface.

## Dependency

```
sudo apt install docker docker-compose
```

## Configuration

Four parameters can be configured in the `idarling_server:environment` section of `docker-compose.yml`:

- `authmodes`: Configure the allowed authentication methods. The available options are `none` (no authentication), `chap` (password authentication), and `pubkey` (public key authentication). E.g. `authmodes = chap pubkey` to allow both methods.
- `sslargs`: Configure SSL parameters. By default, SSL is not enabled. If you need to enable it, you need to provide a valid certificate. E.g. `sslargs =-ssl cert.pem cert.key`
- `log_level`: Configure the output log level such as: WARNING, INFO, DEBUG, etc. E.g. `log_level = DEBUG`
- `autosave_interval`: Configure the interval for automatic saving, that is, how many ticks to save the database. E.g. `autosave_interval = 1000`

## Installation

```
./install.sh
```

## Start

```
./start.sh
```

## Stop

```
./stop.sh
```