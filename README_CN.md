# IDArling Deploy Helper

快速部署 idarling 服务器及管理界面。

## 依赖

- Docker
- Docker Compose

## 配置

在 `docker-compose.yml` 的 idarling_server 中可选四个环境变量，用于配置 idarling 服务器的参数
  - authmodes 配置允许的鉴权方法，可选的选项有 none（无认证）、chap（密码认证）、pubkey（公钥认证），可以多选，用空格分隔即
可，例如：authmodes=chap pubkey。
  - sslargs 配置 ssl 的参数，默认 ssl 不开启，若需要开启，则需提供有效的证书，例：sslargs=--ssl cert.pem cert.key
  - log_level 配置输出日志的等级，有 WARNING、INFO、DEBUG 等可选，（例：log_level=DEBUG）
  - autosave_interval 配置自动保存的间隔，即每多少 tick 保存一次数据库（例：autosave_interval=1000）

## 使用方法

```
# install
./install.sh
# start
./start.sh
# stop
./stop.sh
```